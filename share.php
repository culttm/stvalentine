<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml"> 
<head>
	<meta charset="UTF-8">
	<meta property="og:title" content="<?php echo $_GET["title"]; ?>" />
  	<meta property="og:description" content="<?php echo $_GET["description"]; ?>" />
	<meta property="og:image" content='<?php echo $_GET["image"]; ?>' />
	
</head>
<body>
<script>
	history.back();
</script>
</body>
</html>
