$(function(){    

	$.fn.filterByData = function(prop, val) {
        return this.filter(
            function() { return $(this).data(prop)==val; }
        );
    };

    var artCarousel = $('.num-carousel').owlCarousel({
			            singleItem: true,
			            slideSpeed: 700,
			            mouseDrag:false,
			            pagination:false,
			            touchDrag:true,
			            addClassActive:true,
			            autoHeight:false,
			            nav:false,
			            items:1
			        });


    var App = function(settings){

		this.config = {
			point: $('.nav-pagination .point'),
			button: $('[data-btn-event]'),
			hashTitle: $('[data-title]'), 
			social: $('[data-provider]'), 
			image: $('[data-image]'), 
			onLoad:false,				
			afterChange:false				
		};

		$.extend( this.config, settings ); 	
    };

   	App.prototype = {
        
        init:function(settings){

	        this.setup();
	        this.onLoad();

        },
        setup: function(){

         	var self = this;
         		
	     		self.setVars();
				self.paginationControls();
				self.buttonControls();
				self.updateOnResize();        	 				
 	         	
        }, 
        setVars: function(){

        	this.currentItem = 0;   
 
        },
        buttonControls: function(current){

    		var self = this;

	        	self.config.button.on('mouseup', function(){

			        if($(this).data('btnEvent') == 'next'){
			        	 
			            self.config.point.eq(self.currentItem).next().trigger('mouseup');

			        }else if($(this).data('btnEvent') == 'prev'){
 			            
 			            self.config.point.eq(self.currentItem).last().prev().trigger('mouseup');
			        
			        }

	        	});

        },
        paginationControls:function(){

        	var self = this;

	         	self.config.point.on('mouseup', function(){
	         		self.upDate($(this).index());
	         	});

        },  
        setLineWidth: function(num){

            var el = this.config.point.eq(num),
            	parent = el.parent()
                line = parent.parent().find('.line')
                position = Math.ceil(el.position().left);
                
            line.css('width', position);


        },            
        goTo: function(i, type){
 

			artCarousel.trigger('to.owl.carousel', i);

        },      
        upDate: function(el){

            this.currentItem = el;
            this.updateClasses(el);
            this.goTo(el, 'goTo');
            this.updateButtons(el);
            this.setLineWidth(el);    
            this.onChange();
       
        },

        updateClasses: function(el){

            this.config.point.eq(el).nextAll().removeClass('current');
            this.config.point.eq(el).addClass('current').prevAll().addClass('current');

        },
        updateButtons: function(i){

            var pointsLength = this.config.point.length;

	            this.config.button.removeClass('disabled')

	            if(i == pointsLength - 1){
	                this.config.button.filterByData('btn-event', 'next').addClass('disabled');
	            }

	            if(i == 0){
	                this.config.button.filterByData('btn-event', 'prev').addClass('disabled');
	            } 

        },
        updateOnResize: function(){
        		
        		var self = this;

	         	$(window).on('resize', function(){
	         		var current = self.config.point.eq(self.currentItem).index();
 		         	self.setLineWidth(current);
	         	});
	
        },
        onLoad : function(){

            var self = this,
            	hash = window.location.hash.replace(/#/g,'');

	            if (typeof self.config.onLoad === "function") {
	                self.config.onLoad.apply(this);
	            }

        },
        onChange: function(){
            
            var self = this;

	            if (typeof self.config.onChange === "function") {
	                self.config.onChange.apply(this);
	            }
        }

    };

	var page = new App();
		page.init();


 

 
})