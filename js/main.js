$(function(){

	function inInt(el){

		var $this = $(el),
			items = el.dom.$oItems,
			dot = $(el.dom.$el).find('.owl-controls .owl-dot');

		$.each(items, function(i){
			var item = $(items[i]),
				alt = item.find('img').attr('alt');

				$(dot[i]).html('<div class="dot-text dot-'+(i+1)+'">'+ alt+'</div>') 
 		})

	};		
 
 

	$('.owlcarousel').each(function(){
		var carousel = $(this);

		if(carousel.hasClass('title-pagination')){
 
			carousel.on('initialized.owl.carousel change.owl.carousel changed.owl.carousel', function(e) { 
		        if (!e.namespace || e.type != 'initialized' && e.property.name != 'position') return;
		        
		        var current = e.relatedTarget.current();
		        var items = $(this).find('.owl-stage').children();


		        var image = items.eq(e.relatedTarget.normalize(current)).find('img').attr('src');
		        var title = items.eq(e.relatedTarget.normalize(current)).find('img').attr('alt');

					
				$('[data-share]').each(function(){
					 $(this).attr('data-share-image', image).attr('data-share-title', title);
				});

				$('.share').eq(e.relatedTarget.normalize(current) - 1)
					.removeClass('hide')
					.addClass('visible')
					.siblings()
					.removeClass('visible')
					.addClass('hide');

		    }).owlCarousel({
	 				items:1,
					loop:true,
					nav:true,
  					onInitialized : function(){
						inInt(this);
					}  
	 			});

		}else{
			carousel.owlCarousel({
				items:1,
				loop:true,
				nav:true
			});
		}


	});

 

// Share buttons
	var Share = {
	    odn: function(purl, ptitle) {
	        url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl='+encodeURIComponent(purl);
	        Share.popup(url);
	    },
	    fb: function(purl, ptitle) {
 	        url  = 'http://www.facebook.com/sharer.php?s=100';
	        url += '&p[url]='+ encodeURIComponent(purl);
 	        Share.popup(url);
	    },
	    tw: function(purl, ptitle) {
	        url  = 'http://twitter.com/share?';
	        url += 'text='      + encodeURIComponent(ptitle);
	        url += '&url='      + encodeURIComponent(window.location.origin);
	        url += '&counturl=' + encodeURIComponent(window.location.origin);
	        Share.popup(url);
	    },

	    popup: function(url) {
	        window.open(url,'','toolbar=0,status=0,width=534,height=500');
	    }
	};


	$(document).on('click', '[data-share]', function(e){

		e.preventDefault();

		var locationArray = window.location.pathname.split('/'),
			d;

			locationArray.pop()
		
		var part = locationArray[1];

		if(typeof part !== 'undefined'){
			d = window.location.origin +'/'+ part;
 		}else{
			d = window.location.origin;
 		}

		var type = $(this).data('share'),
			title = $(this).data('share-title'),
			description = $("meta[name='description']").attr('content'),
			image = $(this).data('share-image'),
			purl = d + '/share.php?image='+ d +'/'+ encodeURIComponent(image)+'&title='+encodeURIComponent(title)+'&description='+encodeURIComponent(description);

 			Share[type](purl, title);
			 
	});

 
	if($(window).width() > 480){
 
		$(window).stellar({
			positionProperty: 'transform'
		});
	}

	


	var nav  = $('.main-nav'),
		stickyNavTop = $('.main-nav').offset().top;

	var stickyNav = function(){
		var scrollTop = $(window).scrollTop();
		     
		if (scrollTop > stickyNavTop) { 
		    $('body').addClass('have-fixed');
		    nav.addClass('fixed');
		} else {
			$('body').removeClass('have-fixed');
		    nav.removeClass('fixed'); 
		}
	};

	stickyNav();

	$(window).scroll(function() {
		stickyNav();
	});


 // Modal

	var Modal = (function(jQuery,window,document) {

		var modalWindow,
			modalTrigger = $('.js-modal-show'),

			getModalId = function(el){
				return modalWindow = $('#'+el.data('modal'));
			},

			showModal = function(el){
				$('html').addClass('locked');
				getModalId(el);
  				modalWindow.addClass('show');
			},

			removeModal = function(el){
				$('html').removeClass('locked');
  				modalWindow.removeClass('show');
			},

			setHeight = function(){
				return modalWindow.css('height', $(window).height() + 'px');
			},

			updateHeight = function(){

				$(window).on('resize', function(){
					setHeight();
				});

			},

			initScrollBar = function(){
				setHeight();
				updateHeight();
				modalWindow.find('.content').mCustomScrollbar();
			};

			modalTrigger.on('click', function(e){
				e.preventDefault();
				showModal($(this));
				initScrollBar();


			});

			$(document).on('click', '.close, .md-overlay', function(){
				removeModal();
			});

	})(jQuery,window,document);


 
	
	$('.js-tab').on('click', function(){

		var target = $(this).data('target');
			$(this).addClass('active').siblings().removeClass('active');
			$('#'+target).removeClass('hide').siblings('.iframe-wrapper').addClass('hide');
	});

	
})